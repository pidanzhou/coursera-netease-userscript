// ==UserScript==
// @name         Coursera with Netease
// @namespace    http://github.com/pidanzhou/coursera-netease-userscript
// @version      0.2
// @description  为Coursera课程强制加载网易服务器上的视频文件
// @match        https://class.coursera.org/*/lecture/view*
// @copyright    2014+, RiMo
// @homepageURL  https://github.com/pidanzhou/coursera-netease-userscript
// @updateURL    https://raw2.github.com/pidanzhou/coursera-netease-userscript/master/coursera-netease.user.js
// ==/UserScript==
var x = document.getElementsByTagName("source");
for (var i=0;i<x.length;i++){
	document.getElementsByTagName('source')[i].src = x[i].src.replace(/\/\/.*cloudfront.net\/(.*)\.\w*/,"//v.coursera.126.net/spark-public/$1.mp4");
    document.getElementsByTagName('source')[i].type = "video/mp4";
}